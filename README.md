# Electronics Shop - Rameez Joya

## Requirements
* Node.js: https://nodejs.org/en/
* MongoDB Community Edition: https://docs.mongodb.com/manual/installation/
* Git: https://git-scm.com/

## How to Use
* Clone this repository:
```
git clone https://thinkrameez@bitbucket.org/thinkrameez/electronicsshop.git
```
* Navigate to the application's folder on a terminal and run the application:
```
node index.js
```
* Navigate to the local url where the website is hosted: http://localhost:8080/
* Follow the on-screen instructions to choose items, fill out your information, submit your order, and view all orders

## About License
The MIT license is in effect. The reason behind this is that people may copy this code for educational purposes as long as the license remains.