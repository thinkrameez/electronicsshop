const { validationResult } = require("express-validator"); // Package for validation

//models

const {Order, Admin}  = require('../models');

// constants
const Products = require('../util/products');

var SESSION = require("express-session"); // to store login session

const LOGOUT_MSG = "You are logged out successfully!";
const LOGIN_MSG = "You are logged in successfully!";
const LOGIN_ALREADY_MSG = "You are logged in already!";
const NOT_LOGGED_IN_MSG= "You are not logged in!";
const ORDER_CREATED_MSG = "Order Created successfully!";
const ORDER_DELETED_MSG = "Order Deleted successfully!";
const INVALID_MEMBER_ID = "Invalid Member ID";
const INVALID_REQUEST_MSG = "Invalid request";

//home page
const Home = function (req, res) {
  res.render("front/home", {
    active: 'home',
    loggedIn: SESSION.loggedIn,
    errors: [],
    Products,
    order: new Order({}),
    message: req.query.message
  });
};

//login page
const Login = function (req, res) {
  SESSION = req.session;
  if (!SESSION.loggedIn) {
    res.render("front/login", {
      active: 'login',
      loggedIn: SESSION.loggedIn,
      errors: [],
      Products,
      message: req.query.message
    });
  } else {
    redirectWithMessage("/orders", LOGIN_ALREADY_MSG, res);
  }
};

//validate login details
const LoginValidate = function (req, res) {
  SESSION = req.session;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.render("front/login", {
      errors: errors.array(),
      active: "login",
      loggedIn: SESSION.loggedIn,
    });
  } else {
    const username = req.body.username;
    const password = req.body.password;
    Admin.findOne({ username, password }).exec(function (err, user) {
      // check if user exists
      if (err || !user || !user.username) {
        res.render("front/login", {
          errors: [{ msg: "Invalid account details" }],
          active: "login",
          loggedIn: SESSION.loggedIn,
        });
      } else {
        SESSION = req.session;
        SESSION.loggedIn = true;
        redirectWithMessage("/orders", LOGIN_MSG, res);
      }
    });
  }
};

//logout
const Logout = function (req, res) {
  SESSION = req.session;
  SESSION.loggedIn = false;
  redirectWithMessage("/login", LOGOUT_MSG, res);
};

//list orders
const AllOrders = function (req, res) {
  SESSION = req.session;
  if(SESSION.loggedIn) {
    Order.find({}, function (err, orders) {
      res.render("front/orders", {
        orders,
        Products,
        active: "orders",
        loggedIn: SESSION.loggedIn,
        errors: [],
        message: req.query.message
      });
    });
  } else {
    redirectWithMessage('/login', NOT_LOGGED_IN_MSG, res);
  }

};

//add new order
const CreateOrder = async function (req, res) {
    SESSION = req.session;
    const errors = validationResult(req);
    const orderValues = {
      customerName: req.body.customerName,
      membershipNumber: req.body.membershipNumber,
      doorbells: req.body.doorbells,
      cameras: req.body.cameras,
      monitors: req.body.monitors,
      // slug: slugify(req.body.menu.toLowerCase()),
    };
    
    if (errors.isEmpty()) {
      // check minimum quantity
      const totalQuantity = orderValues.doorbells + orderValues.cameras + orderValues.monitors;
      if(totalQuantity > 0) {
        const order = new Order(orderValues);
        order.save();
        res.render("front/receipt", {
          order: orderValues,
          active: "home",
          loggedIn: SESSION.loggedIn,
          Products,
          errors: [],
          message: ORDER_CREATED_MSG,
        });
      } else {
        res.render("front/home", {
          order: orderValues,
          active: "home",
          loggedIn: SESSION.loggedIn,
          Products,
          errors: [{msg: 'You need to buy at least one product!'}],
        });
      }
    } else {
      let errorsList  = errors.array()
      console.log(errorsList)
      res.render("front/home", {
        order: orderValues,
        active: "home",
        loggedIn: SESSION.loggedIn,
        Products,
        errors: errorsList,
      });
    }
};

// Delete Order
const DeleteOrder = function (req, res) {
  SESSION = req.session;
  if (SESSION.loggedIn) {
    const orderId = req.params.id;
    if (orderId) {
      Order.findOne({ _id: orderId }, function (err, order) {
        if (err) {
          console.log(err); // return to orders list with error
          redirectWithMessage("/orders", INVALID_REQUEST_MSG, res);
        } else {
          // update old one
          Order.deleteOne({ _id: order._id }, function (err, order) {
            if (err) throw err;
            redirectWithMessage("/orders", ORDER_DELETED_MSG, res); // redirect to orders lsit after delete
          });
        }
      });
    } else {
      redirectWithMessage("/orders", INVALID_REQUEST_MSG, res);
    }
  } else {
    redirectWithMessage("/login", NOT_LOGGED_IN_MSG, res);
  }
};

// my orders page
const MyOrders = function (req, res) {
  SESSION = req.session;
  res.render("front/my-orders", {
    orders:[],
    active: "my-orders",
    loggedIn: SESSION.loggedIn,
    errors: [],
    message: req.query.message,
    membershipNumber: ''
  });

};

// get orders by member id
const GetMemberOrders = function (req, res) {
    SESSION = req.session;
    const memberId = req.body.membershipNumber;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.render("front/my-orders", {
        errors: errors.array(),
        active: "my-orders",
        loggedIn: SESSION.loggedIn,
        orders:[],
        membershipNumber: ''
      });
    } else {
      Order.find({membershipNumber: memberId}, function (err, orders) {
        if(err || !orders || orders.length < 0) {
          redirectWithMessage('/my-orders', INVALID_MEMBER_ID, res);
        } else {
          res.render("front/my-orders", {
            orders,
            Products,
            active: "my-orders",
            membershipNumber: memberId,
            loggedIn: SESSION.loggedIn,
            errors: [],        
          });
        }
      });
    }

};

// helper method to redirect with message
function redirectWithMessage(path, message, res) {
  res.redirect(path + "?message=" + encodeURIComponent(message) );  // redirect to login if not logged in
}

module.exports = {
  Home,
  Login,
  AllOrders,
  CreateOrder,
  DeleteOrder,
  LoginValidate,
  Logout,
  MyOrders,
  GetMemberOrders
};
