// ------------------ Fill the following details -----------------------------
// Student name: Rameez Joya
// Student email: rjoya6097@conestogac.on.ca

const express = require('express');
const path = require('path');
const session = require('express-session');
const {check} = require('express-validator');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/final8020set2', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const {Order, Admin}  = require('./models');
var myApp = express();
myApp.use(session({
    secret: 'superrandomsecret',
    resave: false,
    saveUninitialized: true
}));
myApp.use(express.urlencoded({ extended: false }));

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');

//------------- Use this space only for your routes ---------------------------

// controller
const CONTROLLER = require("./controllers/");

// login routes
myApp.get("/", CONTROLLER.Home); 
myApp.get("/login", CONTROLLER.Login); 

// order related routes
const loginValidations = [
    check("username", "Please enter a valid username").notEmpty(),
    check("password", "Please enter a password").notEmpty(),
];
myApp.post("/login", loginValidations, CONTROLLER.LoginValidate); 
myApp.get("/logout", CONTROLLER.Logout); 

const VALID_NAME_REGEX = /^[a-zA-Z]{1,}\s[a-zA-Z\s]+$/;
const VALID_MEMBERSHIP_REGEX = /^[a-zA-Z]{1}[0-9]{2}-[a-zA-Z]{2}[0-9]{1}-[0-9]{3}$/; // A12-BC3-456.
const OrderValdiaton = [
    check("customerName", "Please enter a valid customer name").matches(VALID_NAME_REGEX),
    check("membershipNumber", "Please enter a valid membership number").matches(VALID_MEMBERSHIP_REGEX),
];
const Products = require('./util/products');
for(let product of Products) {
    OrderValdiaton.push(
        check(product.slug, `Please enter valid quantity for ${product.slug}`).isInt()
    )
}
myApp.post("/order", OrderValdiaton, CONTROLLER.CreateOrder); 
myApp.get("/orders", CONTROLLER.AllOrders); 
myApp.get("/order/delete/:id", CONTROLLER.DeleteOrder); 

// my order routes
myApp.get("/my-orders", CONTROLLER.MyOrders); 
myApp.post("/my-orders", 
[check("membershipNumber", "Please enter a valid membership number").matches(VALID_MEMBERSHIP_REGEX)], 
CONTROLLER.GetMemberOrders); 

// write any other routes here as needed

//---------- Do not modify anything below this other than the port ------------
//------------------------ Setup the database ---------------------------------

myApp.get('/setup',function(req, res){
    
    let adminData = [{
        'username': 'admin',
        'password': 'admin'
    }];
    
    Admin.collection.insertMany(adminData);

    var firstNames = ['John ', 'Alana ', 'Jane ', 'Will ', 'Tom ', 'Leon ', 'Jack ', 'Kris ', 'Lenny ', 'Lucas '];
    var lastNames = ['May', 'Riley','Rees', 'Smith', 'Walker', 'Allen', 'Hill', 'Byrne', 'Murray', 'Perry'];

    let ordersData = [];

    for(i = 0; i < 10; i++){
        let tempMemb = 'C'+Math.floor((Math.random() * 100)) + '-ON' + Math.floor((Math.random() * 10)) + '-' + Math.floor((Math.random() * 1000))
        let tempName = firstNames[Math.floor((Math.random() * 10))] + lastNames[Math.floor((Math.random() * 10))];
        let tempOrder = {
            customerName: tempName,
            membershipNumber: tempMemb,
            doorbells: Math.floor((Math.random() * 10)),
            cameras: Math.floor((Math.random() * 10)),
            monitors: Math.floor((Math.random() * 10))
        };
        ordersData.push(tempOrder);
    }
    
    Order.collection.insertMany(ordersData);
    res.send('Database setup complete. You can now proceed with your exam.');
    
});

//----------- Start the server -------------------

myApp.listen(8080);// change the port only if 8080 is blocked on your system
console.log('Server started at 8080 for mywebsite...');