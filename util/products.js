module.exports = [{
    name: "Video Doorbells",
    image: "images/doorbell.png",
    price: 49.98,
    slug: 'doorbells'
},{
    name: "Outdoor Cameras",
    image: "images/cctv-camera.png",
    price: 149.49,
    slug: 'cameras'
},{
    name: "Baby Monitors",
    image: "images/baby-monitor.png",
    price: 99.99,
    slug: 'monitors'
}];