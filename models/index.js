const mongoose = require('mongoose');

const Order = mongoose.model('Order',{
    customerName: String,
    membershipNumber: String,
    doorbells: Number,
    cameras: Number,
    monitors: Number
} );

const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});


module.exports = {
    Order, Admin
};